coord_t __getmousexy() {
	int* __x = NULL;
	int* __y = NULL;
	__asm__ __volatile__ ("movl $3, %eax\n\t"
	                      "movl %0, %%ebx\n\t"
	                      "movl %1, %%ecx\n\t"
	                      "int $0x80"
	                      : /* NULL */
	                      : "X"(__x), "X"(__y));
	return (coord_t){ (*__x), (*__y) };
}

coord_t getmousexy() {
	return __getmousexy();
}

