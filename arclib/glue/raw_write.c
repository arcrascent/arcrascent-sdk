#include <glue/glue.h>

void __raw_write(void* dest, void* src, int len) {
	__asm__ __volatile__ ("movl $0, %eax\n\t"
						  "movl %0, %%ebx\n\t"
						  "movl %1, %%ecx\n\t"
						  "movl %2, %%edx\n\t"
						  "int $0x80"
						  : /* NULL */
						  : "X"(dest), "X"(src), "X"(len));
}

void raw_write(void* dest, void* src, int len) {
	__raw_write(dest, src, len);
}

