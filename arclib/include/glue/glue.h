#ifndef __GLUE_H_INCLUDED__
#define __GLUE_H_INCLUDED__

#include <stdint.h>
#include <stdio.h>
#include <string.h>

extern void raw_write(void* dest, void* src, int len);

#endif
