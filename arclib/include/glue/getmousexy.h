#ifndef __GETMOUSEXY_H_INCLUDED__
#define __GETMOUSEXY_H_INCLUDED__

#include <common.h>

typedef struct __coord_t {
	int x;
	int y;
} coord_t;

#endif
