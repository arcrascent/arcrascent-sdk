#!/bin/bash

if [ -e "make-automake" ]; then
	rm -rf make-automake
fi

if [ -e "make-binutils" ]; then
	rm -rf make-binutils
fi

if [ -e "make-gcc" ]; then
	rm -rf make-gcc
fi

if [ -e "make-gmp" ]; then
	rm -rf make-gmp
fi

if [ -e "make-newlib" ]; then
	rm -rf make-newlib
fi

if [ -e "make-mpc" ]; then
	rm -rf make-mpc
fi

if [ -e "make-mpfr" ]; then
	rm -rf make-mpfr
fi

