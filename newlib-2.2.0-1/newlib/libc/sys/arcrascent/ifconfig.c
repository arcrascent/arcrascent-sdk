#include <sys/syscalls.h>
#include <sys/net.h>
#include <errno.h>
#include <reent.h>

int ifconfig(char* device, struct ifconfig_t* conf, int up) {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "movl %3, %%ecx\n\t"
	                      "movl %4, %%edx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_IFCONFIG), "r" (device),
	                        "r" (conf), "r" (up));
	
	if (ret < 0) {
		_REENT->_errno = -ret;
		return -1;
	}
	
	return ret;
}
