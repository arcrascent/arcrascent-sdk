#ifndef __NET_H
#define __NET_H

struct ifconfig_t
{
	unsigned int ipaddr;
	unsigned int netmask;
	unsigned int broadcast;
	unsigned int mtu;
};

#endif
