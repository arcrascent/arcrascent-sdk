#ifndef __UTSNAME_H
#define __UTSNAME_H

struct utsname
{
	char sysname[64];
	char nodename[65];
	char release[64];
	char version[64];
	char machine[64];
};

#endif
