#ifndef __DIRENT_H
#define __DIRENT_H

typedef struct __dirdesc
{
	int dd_fd;
	long dd_loc;
	long dd_size;
	long dd_len;
	long dd_seek;
	char *dd_buf;
} DIR;

#define __dirfd(dp) ((dp)->dd_fd)

DIR *opendir(const char *file);
int closedir(DIR *dirp);
struct dirent *readdir(DIR *dirdec);

#include <sys/types.h>

#define MAXNAMLEN 255

#define d_ino d_fileno

struct dirent
{
	unsigned long d_ino;
	unsigned long d_off;
	unsigned short d_reclen;
	char d_name[1+MAXNAMLEN];
};

#endif
