#ifndef __NEWLIB_SYSCALLSH
#define __NEWLIB_SYSCALLSH

#define SYS_EXIT		1
#define SYS_OPEN 		2
#define SYS_READ 		3
#define SYS_WRITE 		4
#define SYS_CLOSE 		5
#define SYS_SLEEP 		6
#define SYS_WAKE  		7
#define SYS_YIELD 		8
#define SYS_FSTAT 		9
#define SYS_STAT 		10
#define SYS_SBRK 		11
#define SYS_MOUNT 		12
#define SYS_UNMOUNT 	13
#define SYS_LSEEK 		14
#define SYS_GETDENTS 	15
#define SYS_CHDIR		16
#define SYS_MKDIR      17
#define SYS_IFCONFIG   18
#define SYS_EXECVE     19
#define SYS_FORK       20
#define SYS_UNAME      21
#define SYS_WAIT       22
#define SYS_RENAME     23
#define SYS_DUP        24
#define SYS_REMOVE     25
#endif
