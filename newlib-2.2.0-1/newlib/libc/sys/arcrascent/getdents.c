#include <sys/syscalls.h>
#include <errno.h>
#include <reent.h>

/*                      dp :)           */
int _getdents(int fd, void* dp, int count) {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "movl %3, %%ecx\n\t"
	                      "movl %4, %%edx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_GETDENTS), "r" (fd), "r" (dp), "r" (count));
	
	if (ret < 0) { 
		_REENT->_errno = -ret;
		return -1;
	}
	
	return ret;
}

int getdents(int fd, void* dp, int count) {
	return _getdents(fd, dp, count);
}
