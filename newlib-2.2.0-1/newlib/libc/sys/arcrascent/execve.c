#include <sys/syscalls.h>
#include <errno.h>

int _execve(char* name, char** argv, char** env) {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "movl %3, %%ecx\n\t"
	                      "movl %4, %%edx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_EXECVE), "r" (name),
	                        "r" (argv), "r" (env));
	
	if(ret < 0) {
		_REENT->_errno = -ret;
		return -1;
	}
	
	return ret;
}

int execve(char* name, char** argv, char** env) {
	return _execve(name, argv, env);
}
