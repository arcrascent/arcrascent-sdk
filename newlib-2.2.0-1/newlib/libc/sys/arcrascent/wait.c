#include <sys/syscalls.h>
#include <sys/types.h>
#include <errno.h>
#include <reent.h>

int _wait(int *status) {
  int ret;
  __asm__ volatile("int $0x40" : "=a" (ret) : "0" (SYS_WAIT), "b" (-1), "c" (status), "d" (0));
  if (ret < 0) {
  	_REENT->_errno = -ret;
  	return -1;
  }
  return ret;
}

pid_t waitpid(pid_t pid, int *status, int options) {
  int ret;
  __asm__ volatile("int $0x40" : "=a" (ret) : "0" (SYS_WAIT), "b" (pid), "c" (status), "d" (options));
  if (ret < 0) {
  	_REENT->_errno = -ret;
  	return -1;
  }
  return ret;
}

int wait(int *status) {
	return _wait(status);
}

