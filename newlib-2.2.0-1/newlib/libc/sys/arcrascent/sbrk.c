#include <sys/syscalls.h>

unsigned int _sbrk(int incr) {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_SBRK), "r" (incr));
	
	return ret;
}

unsigned int sbrk(int incr) {
	return _sbrk(incr);
}

