#include <sys/syscalls.h>
#include <errno.h>
#include <reent.h>

#define ARCRASCENT_FORK_REGULAR 0
#define ARCRASCENT_FORK_VIRTUAL 1

int _fork() {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_FORK), "r" (ARCRASCENT_FORK_REGULAR));	
	
	if (ret < 0) {
		_REENT->_errno = -ret;
		return -1;
	}
  
	return ret;
}

int vfork() {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_FORK), "r" (ARCRASCENT_FORK_VIRTUAL));	
	
	if (ret < 0) {
		_REENT->_errno = -ret;
		return -1;
	}
	
	return ret;
}


int fork() {
	return _fork();
}

