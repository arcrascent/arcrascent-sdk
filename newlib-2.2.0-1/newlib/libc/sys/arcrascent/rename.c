#include <sys/syscalls.h>
#include <errno.h>
#include <reent.h>

int _rename(const char* old, const char* new) {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "movl %3, %%ecx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_RENAME), "r" (old), "r" (new));
	
	if (ret < 0) {
		_REENT->_errno = -ret;
		return -1;
	}
	
	return ret;
}

int rename(const char* old, const char* new) {
	return _rename(old, new);
}

