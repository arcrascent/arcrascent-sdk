#include <sys/syscalls.h>

void _exit(int err) {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_EXIT), "r" (err));
	while (1) {
		// wait to be terminated
		// by the arcshell zombie
		// process slayer layer.
	}
}
