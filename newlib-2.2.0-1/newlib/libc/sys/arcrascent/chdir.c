#include <sys/syscalls.h>
#include <sys/stat.h>
#include <errno.h>
#include <reent.h>

int _chdir(const char* s) {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_CHDIR), "r" (s));

	if (ret < 0) {
		_REENT->_errno = -ret;
		return -1;
	}
	
	return 0;
}

int chdir(const char* s) {
	return _chdir(s);
}
