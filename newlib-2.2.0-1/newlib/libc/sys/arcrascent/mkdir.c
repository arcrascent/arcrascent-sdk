#include <sys/syscalls.h>
#include <errno.h>
#include <sys/types.h>
#include <reent.h>

int _mkdir(const char* name, mode_t mode) {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "movl %3, %%ecx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_MKDIR), "r" (name), "r" (mode));
	
	if (ret < 0) {
		_REENT->_errno = -ret;
		return -1;
	}
	
	return 0;
}

// TODO
int _rmdir(const char* name) {
	return -1;
}

int mkdir(const char* name, mode_t mode) {
	return _mkdir(name, mode);
}

int rmdir(const char* name) {
	return _rmdir(name);
}

