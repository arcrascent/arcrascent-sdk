#include <sys/syscalls.h>
#include <errno.h>
#include <reent.h>

int _read(int file, char* ptr, int len) {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "movl %3, %%ecx\n\t"
	                      "movl %4, %%edx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_READ), "r" (file),
	                        "r" (ptr), "r" (len));
	
	if (ret < 0) {
		_REENT->_errno = -ret;
		return -1;
	}
	
	return ret;
}


int read(int file, char* ptr, int len) {
	return _read(file, ptr, len);
}

