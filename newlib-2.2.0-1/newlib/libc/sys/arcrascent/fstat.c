#include <sys/syscalls.h>
#include <sys/stat.h>
#include <errno.h>
#include <reent.h>

int _fstat(int file, struct stat* s) {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "movl %3, %%ecx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_FSTAT), "r" (file), "r" (s));
	
	if (ret < 0) {
		_REENT->_errno = -ret;
		return -1;
	}
	
	//	s->st_mode = S_IFCHR;
	
	return 0;
}


int fstat(int file, struct stat* s) {
	return _fstat(file, s);
}

