#include <sys/utsname.h>
#include <sys/syscalls.h>
#include <errno.h>
#include <reent.h>

int uname(struct utsname* uname) {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_UNAME), "r" (uname));
	
	if (ret < 0) {
		_REENT->_errno = -ret;
		return -1;
	}
	
	return ret;
}

