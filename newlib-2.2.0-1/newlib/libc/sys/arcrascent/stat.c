#include <sys/syscalls.h>
#include <sys/stat.h>
#include <errno.h>
#include <reent.h>

int _stat(const char* file, struct stat* s) {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "movl %3, %%ecx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_STAT), "r" (file), "r" (s));
	
	if (ret < 0) {
		_REENT->_errno = -ret;
		return -1;
	}
	
	return 0;
}

// TODO: Symbolic Links for specific filesystems
int lstat(const char* file, struct stat* s) {
	return _stat(file, s);
}

int stat(const char* file, struct stat* s) {
	return _stat(file, s);
}

