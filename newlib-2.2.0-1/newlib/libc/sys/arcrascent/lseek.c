#include <sys/syscalls.h>
#include <errno.h>
#include <reent.h>

int _lseek(int file, int ptr, int dir) {
	int ret;
	__asm__ __volatile__ ("movl %1, %%eax\n\t"
	                      "movl %2, %%ebx\n\t"
	                      "movl %3, %%ecx\n\t"
	                      "movl %4, %%edx\n\t"
	                      "int $0x40\n\t"
	                      "movl %%eax, %0"
	                      : "=m" (ret)
	                      : "r" (SYS_LSEEK), "r" (file), "r" (ptr), "r" (dir));
	
	if (ret < 0) {
		_REENT->_errno = -ret;
		return -1;
	}
	
	return ret;	
}

int lseek(int fd, int ptr, int dir) {
	return _lseek(fd, ptr, dir);
}

