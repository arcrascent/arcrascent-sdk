#undef TARGET_ARCRASCENT
#define TARGET_ARCRASCENT 1
 
/* Don't automatically add extern "C" { } around header files. */
#undef  NO_IMPLICIT_EXTERN_C
#define NO_IMPLICIT_EXTERN_C 1

#define LIB_SPEC "-lc -lg -lm -lnosys"
 
/* Additional predefined macros. */
#undef TARGET_OS_CPP_BUILTINS
#define TARGET_OS_CPP_BUILTINS()      \
  do {                                \
    builtin_define ("__arcrascent__");      \
    builtin_assert ("system=arcrascent");   \
  } while(0);

