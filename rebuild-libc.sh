#!/bin/bash

# varaibles
export SYSROOT="$HOME/arcrascent"
export PREFIX=/usr
export TARGET=i386-arcrascent
export PATH="$PREFIX/bin:$PATH"

chmod +x newlib-2.2.0-1/configure
mkdir make-newlib
cd make-newlib
../newlib-2.2.0-1/configure --prefix=$PREFIX --target=$TARGET
make all
make DESTDIR=${SYSROOT} install

cp -ar $SYSROOT/usr/i386-arcrascent/* $SYSROOT/usr/

