#!/bin/bash

# to be modified by parameters
installdir="$HOME/arcrascent"

# varaibles
export SYSROOT=$installdir
export PREFIX=/usr
export TARGET=i386-arcrascent
export PATH="$PREFIX/bin:$PATH"

# create root directory
mkdir $SYSROOT

# build binutils
mkdir make-binutils
cd make-binutils
../binutils-2.24/configure --target=$TARGET --prefix=$PREFIX --with-sysroot=$SYSROOT
make all
make DESTDIR=${SYSROOT} install

cd ..

# gmp
sudo apt-get install libgmp3-dev

# mpfr
sudo apt-get install libmpfr-dev libmpfr-doc libmpfr4 libmpfr4-dbg

# mpc
sudo apt-get install libmpc-dev

# newlib setup
CURRDIR=$(pwd)

# make symlinkds (a bad hack) to make stupid newlib to work
cd $HOME/cross/bin/ # this is where the cross compiler (i386-elf-) should be installed
ln i386-elf-ar i386-arcrascent-ar
ln i386-elf-as i386-arcrascent-as
ln i386-elf-gcc i386-arcrascent-gcc
ln i386-elf-gcc i386-arcrascent-cc
ln i386-elf-ranlib i386-arcrascent-ranlib

cd $CURRDIR

# build newlib
./rebuild-libc.sh

# newlib cleanup
cd $HOME/cross/bin/
rm i386-arcrascent-ar
rm i386-arcrascent-as
rm i386-arcrascent-gcc
rm i386-arcrascent-cc
rm i386-arcrascent-ranlib

cd $CURRDIR

# fix directories
cp -ar $SYSROOT/usr/i386-arcrascent/* $SYSROOT/usr/

# build gcc
export CFLAGS=-DTARGET_EXECUTABLE_SUFFIX='".ash"'
export CXXFLAGS=$CFLAGS

mkdir make-gcc
cd make-gcc
../gcc-4.9.2/configure --target=$TARGET --prefix=$PREFIX --with-sysroot=$SYSROOT --enable-languages=c,c++
make all-gcc all-target-libgcc
make DESTDIR=${SYSROOT} install-gcc install-target-libgcc
make all-target-libstdc++-v3
make DESTDIR=${SYSROOT} install-target-libstdc++-v3

cd ..

# rebuild libc with freshly compiled gcc toolchain
rm -rf make-newlib
mkdir make-newlib
cd make-newlib
../newlib-2.2.0-1/configure --prefix=$PREFIX --target=$TARGET
make all
make DESTDIR=${SYSROOT} install

# fix directories
cp -ar $SYSROOT/usr/i386-arcrascent/* $SYSROOT/usr/

cd ..

echo -e "\n#append to \$PATH\nexport PATH=\$PATH:$SYSROOT/bin\n" >> ~/.bashrc

