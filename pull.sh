# 
# Copyright (c) 2013-2015 Joonyoung Lee <0fb1d8@gmail.com>
# 
# All Rights Reserved.
# 
# CONFIDENTIAL AND PROPRIETARY
# Unauthorized access(es), storage(es), manipulation(s), and/or replication(s) strictly prohibited.
# Explicitly written and verified permission(s) required for previously stated actions.
# Redistribution(s) of this material in any shape or form strictly prohibited. Failure to abide by
# the previously stated rules may result in legal consequences.
#
# The notice above is to be included in every copy of this material. Modification(s) and/or removal 
# of the notice above strictly prohibited.
# END OF LEGAL NOTICE
# 
# Module Name:
# pull.sh
# 
# Abstract:
# The SPP routine.
# 
# Author:
# Joonyoung Lee
# 

#!/bin/bash

echo "[+] Stashing Local Changes..."
git stash
echo "Done."

echo "[+] Pulling from Git Repo..."
git pull
echo "Done."

echo "[+] Restoring Local Changes..."
git stash pop
echo "Done."

exit 0

