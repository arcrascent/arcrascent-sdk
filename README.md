#Arcrascent SDK#

**Project Name:**

Arcrascent SDK

**Author**

Joonyoung Lee

**Description:**

The official SDK to be used to develop applications for Arcrascent.

The SDK is planned to include the following components.

- GCC/G++ (v4.9.2) and its dependencies

	- Binutils v2.24
	
	- GMP v6.0.0a
	
	- MPFR v3.1.2
	
	- MPC v1.0.3

- Newlib (Libc) v2.1.0

- Libstdc++ (Libc++) v3

- Python 2.7.x Interpreter

- iVGA Graphics Compositor Library

- VX Framework library

**TODO**:

- Finish implementation of Libc

- Add more options to build script

- Port Libstdc++ for C++ support (Finally!).

**IMPORTANT**:

CONTRIBUTERS: Make sure that when you are pulling changes from this repository, use the SPP (Stash-Pull-Pop) Routine:

```
#!/bin/bash
git stash
git pull
git stash pop

```

**OR**

Run ```./pull.sh```

**See Also:**

[Arcrascent Website](http://sites.google.com/site/arcrascent/)

[Arcrascent Sourceforge Page](http://www.sourceforge.net/p/arcrascent/)

**Developers:**

Joonyoung Lee

**Building:**
Simply run ```./build.sh``` -  it's as easy as that!

*Estimated Build Time:*
Approx. 1-2 hrs. depending on performance of platform.

Note: If one would like to install the SDK in a different directory than the default (~/arcrascent), then simply pass the argument ```--installdir=/path/to/installation/directory``` (e.g. ```./build.sh --installdir=/usr``` would install the SDK in '/usr' directory.).

Note 2: Bootstrapped 'i386-elf-gcc' must be installed in '$HOME/cross' directory. If missing, run:
```
#!/bin/bash
mkdir ~/cross-download
cd ~/cross-download
wget http://newos.org/toolchains/i386-elf-4.9.1-Linux-x86_64.tar.xz
tar xf i386-elf-4.9.1-Linux-x86_64.tar.xz
mkdir ~/cross
cp -ar i386-elf-4.9.1-Linux-x86_64/* ~/cross/
cd ~
rm -rf ~/cross-download
echo "Done."
echo -e "\n#append to \$PATH\nexport PATH=\$PATH:~/cross/bin\n" >> ~/.bashrc
```